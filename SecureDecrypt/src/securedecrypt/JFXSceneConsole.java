package securedecrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

/**
 * @author Edward Boot
 */
public class JFXSceneConsole extends Pane {
    Pane scenePane; 
    SecureDecrypt sd;
    TextArea thisOutput;
    
    public JFXSceneConsole(SecureDecrypt app) {
        sd = app;
        VBox layout = new VBox(5);
        layout.setAlignment(Pos.TOP_LEFT);
        layout.setPadding(new Insets(20,20,20,20));
        
        Label title = new Label("Console");
        title.getStyleClass().add("console-label");
        title.setAlignment(Pos.TOP_LEFT);
        
        thisOutput = new TextArea("Ready");

        thisOutput.getStyleClass().add("console");
        thisOutput.setEditable(false);
        thisOutput.setMaxHeight(150);
        
        
        layout.getChildren().addAll(title, thisOutput);
        
        scenePane = new Pane(layout);
        scenePane.getStylesheets().add("css/styles.css");
        scenePane.getStyleClass().add("console-box");
 
    }
    
    public void setText(String theText)
    {
        String currentText = thisOutput.textProperty().getValue();
        currentText = theText + "\n" + currentText;      
        thisOutput.textProperty().setValue(currentText);
    }
    
    public Pane getPane()
    {
        return scenePane;
    }
}
