package securedecrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Edward Boot
 */
public class JFXWindow extends Application {
    public JFXWindow() {
        super();
        
    }
    
    public void start(Stage primaryStage) throws Exception  {
        primaryStage.setTitle("Testing 123");
        StackPane layout = new StackPane();
        Scene newScene = new Scene(layout, 300, 200);
        primaryStage.show();
    }
}
