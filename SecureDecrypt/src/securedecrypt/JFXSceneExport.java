package securedecrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

/**
 * @author Edward Boot
 */
public class JFXSceneExport extends Pane {
    Pane scenePane; 
    SecureDecrypt sd;
    TextArea thisOutput;
    
    public JFXSceneExport(SecureDecrypt app) {
        sd = app;
        VBox layout = new VBox(5);
        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(20,20,20,20));
        
        Label title = new Label("Output");
        title.getStyleClass().add("title-large");
        title.setAlignment(Pos.CENTER);
        
        thisOutput = new TextArea();
        thisOutput.getStyleClass().add("input-box");
        thisOutput.getStyleClass().add("blue-box");
        thisOutput.setEditable(false);

        
        layout.getChildren().addAll(title, thisOutput);
        
        scenePane = new Pane(layout);
        scenePane.getStylesheets().add("css/styles.css");
 
    }
    
    public void setText(String thisText)
    {
        thisOutput.textProperty().setValue(thisText);
    }
    
    public Pane getPane()
    {
        return scenePane;
    }
}
