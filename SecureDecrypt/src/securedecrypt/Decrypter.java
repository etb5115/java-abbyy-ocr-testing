
package securedecrypt;

/**
 *
 * @author Edward Boot
 */
public class Decrypter {
    public Decrypter()
    {
        
    }
    
    public static String encrypt(String text)
    {
        byte[] textBytes = text.getBytes();
        for (int i = 0; i < textBytes.length; i++)
        {
            int currentByteValue = (int)textBytes[i];
            textBytes[i] = (byte)(currentByteValue > 255 ? currentByteValue - 255 + 2 : currentByteValue + 2);
        }
        String strbyte=new String(textBytes);
        return  strbyte;
    }
    
    public static String decrypt(String text)
    {
        byte[] textBytes = text.getBytes();
        for (int i = 0; i < textBytes.length; i++)
        {
            int currentByteValue = (int)textBytes[i];
            textBytes[i] = (byte)(currentByteValue < 0 ? currentByteValue + 255 - 2 : currentByteValue - 2);
        }
        String strbyte=new String(textBytes);
        return strbyte;
    }
}
