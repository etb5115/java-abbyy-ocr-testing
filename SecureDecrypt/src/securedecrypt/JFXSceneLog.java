package securedecrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import java.io.File;
import java.util.*;
import java.lang.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author Edward Boot
 */
public class JFXSceneLog extends Pane {
    Pane logPane;
    SecureDecrypt sd;
    String ocrFile;
    ListView<String> fileList;
    
    public JFXSceneLog(SecureDecrypt app)
    {
        sd = app;
        VBox layout = new VBox(7);
        layout.autosize();
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.setAlignment(Pos.CENTER);
        
        
        Label titleLabel = new Label("Saved Message Files");
        titleLabel.getStyleClass().add("title");
        titleLabel.getStyleClass().add("padding");
        
        fileList = new ListView<>();
        // fileList.getItems().addAll("testing");
        this.rebaseFileList();
        fileList.setOnMouseClicked(e->loadFile());
        fileList.getStyleClass().add("file-list");
        
        // Add three buttons to the grid
        GridPane grid = new GridPane();      
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setAlignment(Pos.CENTER);
        
        Button importFile = new Button("Import File");
        importFile.getStyleClass().add("blue-button");
        importFile.getStyleClass().add("padding");
        Button deleteMessage = new Button("Delete Message");
        deleteMessage.getStyleClass().add("gray-button");
        deleteMessage.getStyleClass().add("padding");
        
        grid.add(importFile, 0, 0);
        grid.add(deleteMessage, 1, 0);
        
        importFile.setOnAction(e->browserDialog());
        
        layout.getChildren().addAll(titleLabel, fileList, grid);
               
        logPane = new Pane(layout);
        logPane.getStylesheets().add("CSS/styles.css");
        logPane.getStyleClass().add("file-column");
        logPane.autosize();
        
    }
    
    private void loadFile()
    {
        String selectedFile = fileList.getSelectionModel().getSelectedItem();
        ocrFile = System.getProperty("user.dir") + "\\import\\" + selectedFile;
        
        sd.updateImportText("Previously saved file loaded, click 'Commit OCR' to transcribe file...");
        sd.setOcrFile(ocrFile);
    }
    
    private void rebaseFileList()
    {
        fileList.getItems().clear();
        File folder = new File(System.getProperty("user.dir") + "\\import\\" );
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                // System.out.println(file.getName());
                fileList.getItems().addAll(file.getName());
            }
        }
        // fileList.getItems().addAll("testing");
    }
    
    FileChooser fileChooser = new FileChooser();
    File loadedFile;
    
    private void browserDialog()
    {
        fileChooser.setTitle("Browse For Files...");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.home"))
        ); 
        
        loadedFile = fileChooser.showOpenDialog(sd.getStage());
        
        sd.log("File " + loadedFile.getName() + " loaded!");

        CopyOption[] options = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING,
            StandardCopyOption.COPY_ATTRIBUTES
          }; 
        Path FROM = Paths.get(loadedFile.getPath());
        ocrFile = System.getProperty("user.dir") + "\\import\\" + loadedFile.getName();
        Path TO = Paths.get(ocrFile);
        
        try
        {
            Files.copy(FROM, TO, options);
            sd.log("Message file successfully saved to import folder");
            sd.setOcrFile(ocrFile);
            sd.updateImportText("File loaded, click 'Commit OCR' to transcribe file...");
            this.rebaseFileList();
        }
        catch (Exception e)
        {
            sd.log("Error loading file! Message: " + e.toString());
        }
        

    }
   
    
    public Pane getPane()
    {
        return logPane;
    }
}
