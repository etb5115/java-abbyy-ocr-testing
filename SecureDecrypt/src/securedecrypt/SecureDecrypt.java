/*
 * Secure Decrypt, V1.0
 * @Unversity Penn State World Campus
 * @Info This is a secure encrypting and decrypting application for a Penn State World Campus project. For educational use only.
 * @Group IST 440W Section 001, Group 3, Spring 2018
 * @author Edward Boot
 */
package securedecrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.*;


/**
 * Creates the secure decrypt application
 */
public class SecureDecrypt extends Application {
    Stage window;
    Scene decryptionScene;
    String ocrFile;
    OCR ocr = new OCR(this);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
       launch(args);
    }
    
    public Stage getStage()
    {
        return window;
    }
    
    JFXSceneLog sceneLog = new JFXSceneLog(this);
    JFXSceneImport sceneImport = new JFXSceneImport(this);
    JFXSceneExport sceneExport = new JFXSceneExport(this);
    JFXSceneConsole sceneConsole = new JFXSceneConsole(this);
    
    public void start(Stage primaryStage) throws Exception  {
        window = primaryStage;
        window.setTitle("SecureDecrypt");
        
        /*
        File currentDirectory = new File(new File(".").getAbsolutePath());
        System.out.println(currentDirectory.getCanonicalPath());
        System.out.println(currentDirectory.getAbsolutePath());
        */
        
        // Create file menu
        Menu menuFile = new Menu("File");
        Menu menuHelp = new Menu("Help");
        MenuItem exit = new MenuItem("Exit");
        MenuItem print = new MenuItem("Print");
        MenuItem about = new MenuItem("About");
        menuFile.getItems().add(print);
        menuFile.getItems().add(exit);
        menuHelp.getItems().add(about);
        
        // Add all menu items to menu bar
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(menuFile);
        menuBar.getMenus().addAll(menuHelp);
        
        // Set up event listeners for buttons
        exit.setOnAction(e -> System.exit(0));
        about.setOnAction(e-> new JFXAbout());
        
        // Set layout items
        BorderPane borderPane = new BorderPane();
        borderPane.getStylesheets().add("css/styles.css");
        borderPane.getStyleClass().add("main");
        borderPane.setTop(menuBar);
        
        borderPane.setLeft(sceneLog.getPane());
        borderPane.setCenter(sceneImport.getPane());
        borderPane.setRight(sceneExport.getPane());
        borderPane.setBottom(sceneConsole.getPane());
        
        // Create scene and display
        decryptionScene = new Scene(borderPane, 1300, 900);
        window.setScene(decryptionScene);
        window.show();
    } 

    /*
    *   Used to create a simple task for scanning OCR, file locations should have double backslashes
    */
    private void ocrScan(String language, String fileName, String fileOutput)
    {
        
        if (language.equals("")) language = "English";
        
        try
        {
            ocr.performRecognition(language, fileName, fileOutput);
            this.log("OCR recognition successful!");
        }
        catch (Exception e)
        {
            this.log("OCR failed, reason: " + e.toString());
        }
    }

    public void updateImportText(String text)
    {
        sceneImport.setText(text);
    }
    
    public void log(String text)
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        
        sceneConsole.setText(sdf.format(cal.getTime()) + ": " + text);
    }
    
    public void setOcrFile(String theFile)
    {
        this.ocrFile = theFile;
    }
    
    public void commitOCR()
    {
        if (ocrFile.equals("")) 
        {
            this.log("Unable to do OCR, no file loaded");
        }
        else
        {
            this.log("Attempting OCR recognition, please wait...");
            // try { Thread.sleep(500); } catch (Exception e) { }
            this.ocrScan("English", ocrFile, System.getProperty("user.dir") + "\\output\\" +  "output.txt");
            updateImportText(ocr.getOutput());
        }
    }
    
    public void encrypt(String text)
    {
        String value = Decrypter.encrypt(text);
        sceneExport.setText(value);
        this.log("Text successfully encrypted");
    }
    
    public void decrypt(String text)
    {
        String value = Decrypter.decrypt(text);
        sceneExport.setText(value);
        this.log("Text successfully decrypted");
    }
    
    public void clearExport()
    {
        sceneExport.setText("");
    }
}
