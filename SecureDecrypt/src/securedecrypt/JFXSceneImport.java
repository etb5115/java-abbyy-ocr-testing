package securedecrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

/**
 * @author Edward Boot
 */
public class JFXSceneImport extends Pane {
    Pane scenePane; 
    SecureDecrypt sd;
    TextArea thisInput;
    
    public JFXSceneImport(SecureDecrypt secureDecrypt) {
        sd = secureDecrypt;
        VBox layout = new VBox(5);
        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(20,20,20,20));
        
        Label title = new Label("Raw Message");
        title.getStyleClass().add("title-large");
        title.setAlignment(Pos.CENTER);
        
        thisInput = new TextArea();
        thisInput.getStyleClass().add("input-box");
        
        GridPane grid = new GridPane();
        grid.getStyleClass().add("padding");
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(15);
        grid.setVgap(15);
        
        
        Button ocr = new Button("commit OCR");
        ocr.getStyleClass().add("gray-button");
        grid.add(ocr, 0, 0);
        
        ocr.setOnAction(e->sd.commitOCR());
        
        Button textClear = new Button("Clear");
        textClear.getStyleClass().add("gray-button");
        grid.add(textClear, 1, 0);
        textClear.setOnAction(e->clearScreen());
        
        Button decrypt = new Button("DECRYPT");
        decrypt.getStyleClass().add("blue-button");
        grid.add(decrypt, 2, 0);
        decrypt.setOnAction(e->decryptText());
        
        Button encrypt = new Button("ENCRYPT");
        encrypt.getStyleClass().add("red-button");
        grid.add(encrypt, 3, 0);
        encrypt.setOnAction(e->encryptText());
        
        layout.getChildren().addAll(title, thisInput, grid);
        
        scenePane = new Pane(layout);
        scenePane.getStylesheets().add("css/styles.css");       
    }
   
    private void encryptText()
    {
       String text = thisInput.textProperty().getValue();
       sd.encrypt(text);
    }
    
    private void decryptText()
    {
        String text = thisInput.textProperty().getValue();
        sd.decrypt(text);
    }
    
    public void clearScreen()
    {
        thisInput.textProperty().setValue("");
        sd.clearExport();
    }
    
    public void setText(String thisText)
    {
        thisInput.textProperty().setValue(thisText);
    }
    
    public Pane getPane()
    {
        return scenePane;
    }
}
