package securedecrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
/**
 *
 * @author Edward Boot
 */
public class JFXAbout {
    public JFXAbout() {
        super();
        Stage popup = new Stage();
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.setTitle("About SecureDecrypt");
        popup.setMinWidth(250);      
        
        Label message = new Label("IST 440W Project, Section 001, Group 03, Spring 2018");
        Label names = new Label("Eric McMullen (Group Leader)\n" +
                                "Christopher Grant\n" +
                                "Ashlee Hammonds\n" +
                                "Edward Boot");
        
        message.getStyleClass().add("heading");
        
        Button closeButton = new Button("Close");
        closeButton.setOnAction(e->popup.close());    
        closeButton.getStyleClass().add("blue-button");
        
        VBox layout = new VBox(10);
        
        layout.getChildren().addAll(message, names, closeButton);
        layout.setAlignment(Pos.CENTER);
        layout.getStyleClass().add("padding");
        
        Scene scene = new Scene(layout);
        scene.getStylesheets().add("CSS/styles.css");
        popup.setScene(scene);
        popup.show();
        
    }
}
