package securedecrypt;

import com.abbyy.ocrsdk.BusCardSettings;
import com.abbyy.ocrsdk.Client;
import com.abbyy.ocrsdk.ProcessingSettings;
import com.abbyy.ocrsdk.Task;
import java.util.Vector;
import java.util.*;
import java.lang.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.Charset;


/**
 *
 * @author Edward Boot
 */
public class OCR {
    private static Client restClient;
    private static String output;
    static SecureDecrypt sd;
    
    public OCR(SecureDecrypt app)
    {
        sd = app;
        ClientSettings.setupProxy();
		
        restClient = new Client();
        // replace with 'https://cloud.ocrsdk.com' to enable secure connection
        restClient.serverUrl = "http://cloud.ocrsdk.com";
        restClient.applicationId = ClientSettings.APPLICATION_ID;
        restClient.password = ClientSettings.PASSWORD;
    }
    
    public String getOutput()
    {
        String result = "";
        // return restClient.getOutput();
        
        String outputPath = System.getProperty("user.dir") + "\\output\\" +  "output.txt";
        Path thisPath = Paths.get(outputPath);
        
        try
        {
            byte[] encoded = Files.readAllBytes(thisPath);
            result =  new String(encoded, Charset.defaultCharset());
        }
        catch (Exception e)
        {
            sd.log("Error loading file output: " + e.toString());
        }

        
        return result;
    }
    

    public void performRecognition(String language, String file, String outputPath) throws Exception 
    {
        ProcessingSettings.OutputFormat outputFormat = outputFormatByFileExt(outputPath);

        ProcessingSettings settings = new ProcessingSettings();
        settings.setLanguage(language);
        settings.setOutputFormat(outputFormat);
        Task task = null;
        
        // sd.log("Uploading file..");
        task = restClient.processImage(file, settings);
        
        /*
        if (argList.size() == 1) {
                sd.log("Uploading file..");
                task = restClient.processImage(argList.elementAt(0), settings);

        } else if (argList.size() > 1) {

                // Upload images via submitImage and start recognition with
                // processDocument
                for (int i = 0; i < argList.size(); i++) {
                        sd.log(String.format("Uploading image %d/%d..",
                                        i + 1, argList.size()));
                        String taskId = null;
                        if (task != null) {
                                taskId = task.Id;
                        }

                        Task result = restClient.submitImage(argList.elementAt(i),
                                        taskId);
                        if (task == null) {
                                task = result;
                        }
                }
                task = restClient.processDocument(task.Id, settings);

        } else {
                sd.log("No files to process.");
                return;
        }
        */

        waitAndDownloadResult(task, outputPath);
    }
    
    /** 
    * Wait until task processing finishes
    */
    private static Task waitForCompletion(Task task) throws Exception {
        // Note: it's recommended that your application waits
        // at least 2 seconds before making the first getTaskStatus request
        // and also between such requests for the same task.
        // Making requests more often will not improve your application performance.
        // Note: if your application queues several files and waits for them
        // it's recommended that you use listFinishedTasks instead (which is described
        // at http://ocrsdk.com/documentation/apireference/listFinishedTasks/).
        while (task.isTaskActive()) {

                Thread.sleep(5000);
                sd.log("Waiting..");
                task = restClient.getTaskStatus(task.Id);
        }
        return task;
    }
	
    /**
     * Wait until task processing finishes and download result.
     */
    private static void waitAndDownloadResult(Task task, String outputPath) throws Exception 
    {
        task = waitForCompletion(task);

        if (task.Status == Task.TaskStatus.Completed) {
                sd.log("Downloading..");
               restClient.downloadResult(task, outputPath);
                sd.log("Ready");
        } else if (task.Status == Task.TaskStatus.NotEnoughCredits) {
                sd.log("Not enough credits to process document. " + "Please add more pages to your application's account.");
        } else {
                sd.log("Task failed");
        }
    }
    
    /**
    * Extract output format from extension of output file.
    */
    private static ProcessingSettings.OutputFormat outputFormatByFileExt(String filePath) {
        int extIndex = filePath.lastIndexOf('.');
        if (extIndex < 0) {
                System.out
                                .println("No file extension specified. Plain text will be used as output format.");
                return ProcessingSettings.OutputFormat.txt;
        }
        String ext = filePath.substring(extIndex).toLowerCase();
        if (ext.equals(".txt")) {
                return ProcessingSettings.OutputFormat.txt;
        } else if (ext.equals(".xml")) {
                return ProcessingSettings.OutputFormat.xml;
        } else if (ext.equals(".pdf")) {
                return ProcessingSettings.OutputFormat.pdfSearchable;
        } else if (ext.equals(".docx")) {
                return ProcessingSettings.OutputFormat.docx;
        } else if (ext.equals(".rtf")) {
                return ProcessingSettings.OutputFormat.rtf;
        } else {
                System.out
                                .println("Unknown output extension. Plain text will be used.");
                return ProcessingSettings.OutputFormat.txt;
        }
    }

    /**
    * Extract output format for business card from extension of output file.
    */
    private static BusCardSettings.OutputFormat bcrOutputFormatByFileExt(String filePath) {
        int extIndex = filePath.lastIndexOf('.');
        if (extIndex < 0) {
                System.out
                                .println("No file extension specified. vCard will be used as output format.");
                return BusCardSettings.OutputFormat.vCard;
        }

        String ext = filePath.substring(extIndex).toLowerCase();
        if (ext.equals(".vcf")) {
                return BusCardSettings.OutputFormat.vCard;
        } else if (ext.equals(".xml")) {
                return BusCardSettings.OutputFormat.xml;
        } else if (ext.equals(".csv")) {
                return BusCardSettings.OutputFormat.csv;
        }

        sd.log("Invalid file extension. vCard will be used as output format.");
        return BusCardSettings.OutputFormat.vCard;
    }
}
